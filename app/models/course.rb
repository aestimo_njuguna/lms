class Course < ActiveRecord::Base
	has_many :payments, inverse_of: :course 
	has_many :taught_classes

end
