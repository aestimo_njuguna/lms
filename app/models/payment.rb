class Payment < ActiveRecord::Base
	belongs_to :course, inverse_of: :payments
	belongs_to :student, inverse_of: :payments

	scope :cash_payments, -> { where(payment_type: 'Cash') }
	scope :Mpesa_payments, -> { where(payment_type: 'Mpesa') }
end
