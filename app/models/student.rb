class Student < ActiveRecord::Base
	has_many :payments, inverse_of: :student
end
