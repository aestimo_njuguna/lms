ActiveAdmin.register TaughtClass do

	menu parent: "Course Management"

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :course_id, :trainer_id, :commence_date, :end_date

#action items, member actions, collection actions & modified controller actions


#show page
show do
	tabs do
		tab 'Taught class' do
			attributes_table do
            	row(:course_id)
            	row(:trainer_id)
            	row(:commence_date)
            	row(:end_date)
          	end
		end
		tab 'Enrolled Students' do
		end
	end
end


#the index page

index do
	selectable_column
	column "Taught course", :course_id do |t|
		t.course.name
	end
	column "Trainer", :trainer_id do |t|
		t.trainer.name
	end
	column :commence_date
	column :end_date

	actions
end	

#the filters
filter :course_id, as: :select, collection: proc {Course.all}
filter :trainer_id, as: :select, collection: proc {Trainer.all}

#the input forms

form do |f|
    inputs 'Details' do
      input :course_id, as: :select, collection: Course.all, label: "Course"
      input :trainer_id, as: :select, collection: Trainer.all, label: "Trainer"
      input :commence_date, as: :datepicker, datepicker_options: { min_date: "2015-1-1", max_date: "+14D" }
      input :end_date, as: :datepicker, datepicker_options: { min_date: "2015-1-1", max_date: "+50D" }
      li "Created at #{f.object.created_at}" unless f.object.new_record?
    end
    actions
 end


end
