ActiveAdmin.register Trainer do
  menu parent: "User Management"

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :national_id, :sex, :employment_date, :termination_date, :speciality

#the index page

index do
	selectable_column
	column "Trainer name", :name
	column :sex
	column :employment_date
	column :termination_date
	column "Trainer ID no.", :national_id
	column :speciality

	actions
end	

#the filters
filter :name, as: :string
filter :national_id

#the input forms

form do |f|
    inputs 'Details' do
      input :name
      input :sex, as: :select, collection: %w[Male Female], label: "Gender"
      input :employment_date, as: :datepicker, datepicker_options: { min_date: "2012-1-1", max_date: "+3D" }
      input :termination_date, as: :datepicker, datepicker_options: { min_date: "2012-1-1", max_date: "+3D" }
      input :national_id
      input :speciality
      li "Created at #{f.object.created_at}" unless f.object.new_record?
    end
    actions
 end

end
