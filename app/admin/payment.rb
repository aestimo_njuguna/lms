ActiveAdmin.register Payment do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :course_id, :student_id, :payment_date, :payment_type, :amount

scope :cash_payments
scope :Mpesa_payments

#batch actions, action items e.t.c


batch_action :enroll_students do |ids|
	#we need to filter this batch action to only work on unique payments...and to remove
   batch_action_collection.find(ids).each do |payment|
     
     #course_paid_for = payment.course_id #gets the course the student paid for..

     student_paying = payment.student_id #gets the student
     taught_class_paid_for = payment.course.taught_classes.pluck(:id) #get the taught class id

     #enroll student in the taught class (corresponding to the course they've paid for)

     new_enrollment = Enrollment.new({student_id: student_paying, taught_class_id: taught_class_paid_for })
     new_enrollment.save 
   end
   redirect_to collection_path, alert: "The students have been enrolled successfully."
end


action_item :enrolled_students, only: :show do
  link_to 'Enrolled students', '/'
end

#the index
index do
	selectable_column
	column "Course", :course_id do |c|
		c.course.name
	end
	column "Student", :student_id do |s|
		s.student.name
	end
	column :payment_date
	column :payment_type
	column :amount

	actions
end

#the filters
filter :payment_date
#filter :student_id, as: :select, collection: proc { Student.all }

#the form
form do |f|
    inputs 'Details' do
      input :course_id, as: :select, collection: Course.all
      input :student_id, as: :select, collection: Student.all
      input :payment_date, as: :datepicker, datepicker_options: { min_date: "2014-10-8", max_date: "+3D" }
      input :payment_type, as: :select, collection: %w[Cash Mpesa Cheque Bank-wire]
      input :amount
    end
    actions
end


end