ActiveAdmin.register Student do
  menu parent: "User Management"

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name,:sex,:birth_date,:national_id,:mobile,:email

#batch actions..custom controllers...etc

collection_action :list_of_paid_up_students, method: :get do
  #we should use this collection method to first get the list of paid up students...
  #...then pass the enrollement action if their total payments for each course..
  #...are greater than the deposit amount
    
end

action_item :paid_students_list, only: :index do
  link_to 'List of paid students', list_of_paid_up_students_admin_students_path
end

controller do

  def list_of_paid_up_students
    #get list of all students with payments
    @all_students = Student.joins(:payments).uniq

    #then render the page
    render
  end

end

#the index page

index do
	selectable_column
	column "Student name", :name
	column :sex
	column :birth_date
	column "Student ID no.", :national_id
	column :mobile
	column :email

	actions
end	

#the filters
filter :name, as: :string
filter :national_id

#the input forms

form do |f|
    inputs 'Details' do
      input :name
      input :sex, as: :select, collection: %w[Male Female], label: "Gender"
      input :birth_date, as: :datepicker, datepicker_options: { min_date: "1980-10-8", max_date: "+3D" }
      input :national_id
      input :mobile
      input :email
      li "Created at #{f.object.created_at}" unless f.object.new_record?
    end
    panel 'Markup' do
      "The following can be used in the content below..."
    end
    inputs 'Full names', :name
    para "Press cancel to return to the list without saving."
    actions
 end


end
