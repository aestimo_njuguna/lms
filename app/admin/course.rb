ActiveAdmin.register Course do
  menu parent: "Course Management"

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :code, :description, :cost

#the index page

index do
	selectable_column
	column "Course", :name
	column :code
	column :description
	column :cost

	actions
end

#the filter

filter :name, as: :string
filter :code, as: :string

#the input forms

form do |f|
    inputs 'Details' do
      input :name
      input :code
      input :description
      input :cost, hint: "The Course cost in Ksh."
    end
    actions
end

end
