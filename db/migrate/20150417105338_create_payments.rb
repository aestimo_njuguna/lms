class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :course_id
      t.integer :student_id
      t.date :payment_date
      t.string :payment_type
      t.integer :amount

      t.timestamps null: false
    end
  end
end
