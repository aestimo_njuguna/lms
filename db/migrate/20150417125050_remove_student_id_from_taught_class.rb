class RemoveStudentIdFromTaughtClass < ActiveRecord::Migration
  def up
  	remove_column :taught_classes, :student_id
  end

  def down
  	add_column :taught_classes, :student_id
  end
end
