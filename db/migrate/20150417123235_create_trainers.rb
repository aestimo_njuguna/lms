class CreateTrainers < ActiveRecord::Migration
  def change
    create_table :trainers do |t|
      t.string :name
      t.integer :national_id
      t.string :sex
      t.date :employment_date
      t.date :termination_date
      t.string :speciality

      t.timestamps null: false
    end
  end
end
