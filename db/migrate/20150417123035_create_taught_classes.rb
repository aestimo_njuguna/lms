class CreateTaughtClasses < ActiveRecord::Migration
  def change
    create_table :taught_classes do |t|
      t.integer :course_id
      t.integer :student_id
      t.integer :trainer_id
      t.date :commence_date
      t.date :end_date

      t.timestamps null: false
    end
  end
end
