class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.integer :student_id
      t.integer :taught_class_id

      t.timestamps null: false
    end
  end
end
